#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h> 
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h> 
#include <sys/stat.h>

int is_number(char* input){
  int length = strlen (input);
  for (int i=0;i<length; i++)//iteramos sobre el string
    if (!isdigit(input[i]))//si el char en turno no es digito
      return -1;//regresamos -1 ya que no es un numero
  return 1;//termino el for asi que todos fueron numeros
}

//read and print from a path file
void read_file(char* file){
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  fp = fopen(file, "r");
  if (fp == NULL)
      return;

  while ((read = getline(&line, &len, fp)) != -1) 
      printf("  %s", line);
 
  printf("\n");
  fclose(fp);
  if (line)
      free(line);
}

//Count of the files in a specified directory
int count_files(char* directory){
  struct dirent *de;  // Pointer for directory entry 
  int count = 0; 
  // opendir() returns a pointer of DIR type.  
  DIR *dr = opendir(directory); 
  if (dr == NULL)  // opendir returns NULL if couldn't open directory 
  { 
    printf("Could not open the directory\n" ); 
    return 0; 
  } 
 
  while ((de = readdir(dr)) != NULL)
    count++;
  return count;
}
char* get_path(char* sympath){
  struct stat sb;
  ssize_t r = INT_MAX;
  int linkSize = 0;
  const int growthRate = 255;

  char* linkTarget = NULL;

  // get length of the pathname the link points to
  if (lstat(sympath, &sb) == -1) {   // could not lstat: insufficient permissions on directory?
      return "";
  }

  // read the link target into a string
  linkSize = sb.st_size + 1 - growthRate;
  while (r >= linkSize) { // i.e. symlink increased in size since lstat() or non-POSIX compliant filesystem
      // allocate sufficient memory to hold the link
      linkSize += growthRate;
      free(linkTarget);
      linkTarget = malloc(linkSize);
      if (linkTarget == NULL) {           // insufficient memory
          fprintf(stderr, "insufficient memory\n");
          return "";
      }

      // read the link target into variable linkTarget
      r = readlink(sympath, linkTarget, linkSize);
      if (r < 0) {        // readlink failed: link was deleted?
          return "";
      }
  }
  linkTarget[r] = '\0';   // readlink does not null-terminate the string
  return linkTarget;
}

/*
  * proc/[pid]/cmdline
  * This holds the complete command line for the process, unless the process is a zombie. 
  * In either of these later cases, there is nothing in this file: i.e. a read on this file will return 0 characters. 
  *
  * proc/[pid]/cwd
  * This is a link to the current working directory of the process.
  *
  * proc/[pid]/environ
  * This file contains the environment for the process.
  *
  * proc/[pid]/exe
  * The exe file is a symbolic link containing the actual path name of the executed command.
  *
  * proc/[pid]/fd
  * This is a subdirectory containing one entry for each file which the process has opened, 
  * named by its file descriptor, and which is a symbolic link to the actual file.
  *
  * proc/[pid]/root
  * This is a link to the root directory which is seen by the process.
  *
  * proc/[pid]/stat
  * This file provides status information about the process.
  *
  * proc/[pid]/comm 
  * This file containing the name of the executable for that process.
*/
int main(void) { 
  struct dirent *de;  // Pointer for directory entry 
  
  // opendir() returns a pointer of DIR type.  
  DIR *dr = opendir("/proc/"); 
  if (dr == NULL)  // opendir returns NULL if couldn't open directory 
  { 
    printf("Could not open current directory\n" ); 
    return 0; 
  } 
  
  // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html 
  // for readdir() 
  while ((de = readdir(dr)) != NULL){
    //getting only the proccess files
    if(is_number(de->d_name) != 1)
      continue;
    //skipping the current thread;
    if(getpid() == atoi(de->d_name)){
      continue;
    }

    
    
    printf("Process ID: %s\n", de->d_name);
    //proc/[pid]/exe
    char exe[strlen(de->d_name) + 10];
    memset(exe, 0, sizeof(exe));
    strcat(exe, "/proc/");
    strcat(exe, de->d_name);
    strcat(exe, "/exe\0");
    printf("Exe path: %s\n", get_path(exe));
    
    char fd[strlen(de->d_name) + 10];
    memset(fd, 0, sizeof(fd));
    strcat(fd, "/proc/");
    strcat(fd, de->d_name);
    strcat(fd, "/fd/\0");
    int count = count_files(fd);
    printf("Number of files opened by the process: %d\n", count);
    //proc/[pid]/cmdline
    char cmdline[strlen(de->d_name) + 14];
    memset(cmdline, 0, sizeof(cmdline));
    strcat(cmdline, "/proc/");
    strcat(cmdline, de->d_name);
    strcat(cmdline, "/cmdline\0");
    //read file
    printf("Cmdline:\n");
    read_file(cmdline);

    char cwd[strlen(de->d_name) + 10];
    memset(cwd, 0, sizeof(cwd));
    strcat(cwd, "/proc/");
    strcat(cwd, de->d_name);
    strcat(cwd, "/cwd\0");
    printf("Cwd path: %s\n", get_path(cwd));
    

    char environ[strlen(de->d_name) + 14];
    memset(environ, 0, sizeof(environ));
    strcat(environ, "/proc/");
    strcat(environ, de->d_name);
    strcat(environ, "/environ\0");
    //read file
    printf("Environ:\n");
    read_file(environ);

    char maps[strlen(de->d_name) + 11];
    memset(maps, 0, sizeof(maps));
    strcat(maps, "/proc/");
    strcat(maps, de->d_name);
    strcat(maps, "/maps\0");
    //read file
    printf("Maps:\n");
    read_file(maps);

    char root[strlen(de->d_name) + 11];
    memset(root, 0, sizeof(root));
    strcat(root, "/proc/");
    strcat(root, de->d_name);
    strcat(root, "/root\0");
    printf("Root path: %s\n", get_path(root));
    

    char stat[strlen(de->d_name) + 11];
    memset(stat, 0, sizeof(stat));
    strcat(stat, "/proc/");
    strcat(stat, de->d_name);
    strcat(stat, "/stat\0");
    //read file
    printf("Stat:\n");
    read_file(stat);

    //cleaning 
    memset(exe, 0, sizeof(exe));
    memset(cmdline, 0, sizeof(cmdline));
    memset(cwd, 0, sizeof(cwd));
    memset(environ, 0, sizeof(environ));
    memset(stat, 0, sizeof(stat));
    memset(maps, 0, sizeof(maps));
    memset(root, 0, sizeof(root));
    printf("------------------------------------------\n");
  }
  
  closedir(dr);     
  return 0; 
} 
